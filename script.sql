DROP DATABASE IF EXISTS exercise4;
CREATE DATABASE exercise4;
USE exercise4;

-- Creating tables.

DROP TABLE IF EXISTS users;
CREATE TABLE users (
  user_id INT AUTO_INCREMENT,
  user_name VARCHAR(30) NOT NULL,
  user_type ENUM('admin','normal'),
  PRIMARY KEY (user_id),
  INDEX index_users (user_id, user_name)
);

DROP TABLE IF EXISTS comments;
CREATE TABLE comments (
  comment_id INT AUTO_INCREMENT,
  user_id INT,
  article_id INT,
  comment_text VARCHAR(120) NOT NULL,
  PRIMARY KEY (comment_id)
);

DROP TABLE IF EXISTS articles;
CREATE TABLE articles (
  article_id INT AUTO_INCREMENT,
  user_id INT,
  article_category VARCHAR(20) NOT NULL,
  article_text MEDIUMBLOB,
  PRIMARY KEY (article_id),
  INDEX index_articles (article_id, user_id, article_category)
);

-- Ensuring referential Integrity.

ALTER TABLE comments
ADD CONSTRAINT fk_CommentsUsers FOREIGN KEY (user_id) REFERENCES users(user_id),
ADD CONSTRAINT fk_CommentsArticles FOREIGN KEY (article_id) REFERENCES articles(article_id);

ALTER TABLE articles
ADD CONSTRAINT fk_ArticlesUsers FOREIGN KEY (user_id) REFERENCES users(user_id);

DESCRIBE users;
DESCRIBE comments;
DESCRIBE articles;

-- Loading test data in tables.

INSERT INTO users(user_name, user_type) VALUES ('user1','admin');
INSERT INTO users(user_name, user_type) VALUES ('user2','normal');
INSERT INTO users(user_name, user_type) VALUES ('user3','normal');
INSERT INTO users(user_name, user_type) VALUES ('user4','normal');
INSERT INTO users(user_name, user_type) VALUES ('user5','normal');

INSERT INTO articles(user_id, article_category, article_text) VALUES (1, 'Category1', 'This can be a large blob for article1');
INSERT INTO articles(user_id, article_category, article_text) VALUES (2, 'Category2', 'This can be a large blob for article2');
INSERT INTO articles(user_id, article_category, article_text) VALUES (3, 'Category3', 'This can be a large blob for article3');
INSERT INTO articles(user_id, article_category, article_text) VALUES (3, 'Category4', 'This can be a large blob for article4');
INSERT INTO articles(user_id, article_category, article_text) VALUES (5, 'Category5', 'This can be a large blob for article5');

INSERT INTO comments(user_id, article_id, comment_text) VALUES (1, 1, 'This is comment 1');
INSERT INTO comments(user_id, article_id, comment_text) VALUES (2, 2, 'This is comment 2');
INSERT INTO comments(user_id, article_id, comment_text) VALUES (3, 3, 'This is comment 3');
INSERT INTO comments(user_id, article_id, comment_text) VALUES (4, 3, 'This is comment 4');
INSERT INTO comments(user_id, article_id, comment_text) VALUES (5, 5, 'This is comment 5');
-- Displaying data in each table.

SELECT * FROM users;
SELECT * FROM comments;
SELECT * FROM articles;

-- select all articles whose author's name is user3
SELECT * FROM articles WHERE user_id IN (SELECT user_id FROM users WHERE user_name = 'user3');

-- select all articles whose author's name is user3 (Do this exercise using variable also)
SELECT GROUP_CONCAT(user_id) into @USER FROM users WHERE user_name = 'user3';
SELECT * FROM articles WHERE user_id = @USER;

-- For all the articles being selected above, select all the articles and also the comments associated with those articles in a single query.
SELECT articles.article_text, comments.comment_text
FROM articles JOIN comments JOIN users
WHERE  ((articles.article_id = comments.article_id) AND (articles.user_id = users.user_id) AND (users.user_name = 'user3'));

-- For all the articles being selected above, select all the articles and also the comments associated with those articles in a single query (Do this using subquery also)
SELECT articles.article_text, comments.comment_text
FROM articles JOIN comments
WHERE  ((articles.article_id = comments.article_id) AND (articles.user_id = (SELECT user_id FROM users WHERE user_name = 'user3')));

-- Write a query to select all articles which do not have any comments
SELECT article_text FROM articles
LEFT JOIN comments
ON articles.article_id = comments.article_id
WHERE comments.comment_id IS NULL ;

-- Write a query to select all articles which do not have any comments (Do using subquery also)
SELECT article_text FROM articles
WHERE article_id NOT IN (SELECT DISTINCT article_id FROM comments);

-- Write a query to select article which has maximum comments.
SELECT article_id, COUNT(*) INTO @a_id, @count FROM comments GROUP BY article_id ORDER BY 2 DESC LIMIT 1;
SELECT article_text FROM articles WHERE article_id = @a_id;

-- Write a query to select article which does not have more than one comment by the same user ( do this using left join and group by )
SELECT article_text
FROM articles LEFT JOIN comments
ON articles.article_id = comments.article_id
GROUP BY articles.article_id, articles.user_id
HAVING COUNT(*) <= 1;